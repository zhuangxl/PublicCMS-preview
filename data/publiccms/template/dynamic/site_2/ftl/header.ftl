<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
<link rel="stylesheet" type="text/css" href="${site.resourcePath}css/common.css" />
<link rel="stylesheet" type="text/css" href="${site.resourcePath}css/layout.css" />
<!--[if lt IE 9]>
<script type="text/javascript" src="${site.resourcePath}js/html5.js"></script>
<script type="text/javascript" src="${site.resourcePath}js/respond.min.js"></script>
<![endif]-->
<script src="http://libs.baidu.com/jquery/1.9.1/jquery.min.js"></script>
<script src="${site.resourcePath}js/jquery.event.move.js"></script>
<script src="${site.resourcePath}js/jquery.event.swipe.js"></script>
<script src="${site.resourcePath}js/support.js"></script>