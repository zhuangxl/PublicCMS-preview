	<footer>
		<div class="footer-scroller-controller">
			<div><a href="javascript:void(0)" class="previous">↑</a></div>
			<div><a href="javascript:void(0)" class="next">↓</a></div>
		</div>
		<div class="footer-scroller">
			<p class="text-center copyright">©${.now?string('yyyy')} sanluan 京ICP备15009690号</p>
			<p class="text-center copyright">有任何问题，您可以<a href="${base}/guestbook.html" class="tools">告诉我们</a></p>
		</div>
	</footer>