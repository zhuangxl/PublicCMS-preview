package com.publiccms.common.constants;

public class CommonConstants {
    public static final String SESSION_USER = "S_USER";
    public static final String SESSION_USER_TIME = "S_USER_TIME";
    public static final String SESSION_ADMIN = "S_ADMIN";
    public static final String COOKIES_USER = "C_USER";
    public static final String COOKIES_USER_SPLIT = "##";
    public static final String DEFAULT_PAGE_BREAK_TAG = "_page_break_tag_";
    public static final String X_POWERED = "X-Powered-PublicCMS";
    public static final String PUBLICCMS_VERSION = CmsVersion.VERSION;
}
